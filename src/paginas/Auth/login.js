import React from "react";
import { Link } from "react-router-dom";

const Login = ()=> {
    return (
        <div class="hold-transition login-page">
            <div className="login-box">
            <div className="login-logo">
                <Link to ={"#"}><b>Inicio de Sesión</b> Tienda</Link>
            </div>
            {/* /.login-logo */}
            <div className="card">
                <div className="card-body login-card-body">
                <p className="login-box-msg">Inicio de sesión</p>
                <form action="../../index3.html" method="post">

                    {/** ****************************texto de email */}
                    <div className="input-group mb-3">
                        <input type="email" className="form-control" placeholder="Email" />
                        <div className="input-group-append">
                            <div className="input-group-text">
                                <span className="fas fa-envelope" />
                            </div>
                        </div>
                    </div>

                    {/** ****************************texto de password */}
                    <div className="input-group mb-3">
                        <input type="password" className="form-control" placeholder="Password" />
                        <div className="input-group-append">
                            <div className="input-group-text">
                                <span className="fas fa-lock" />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                    <div className="col-8">
                        <div className="icheck-primary">
                            <input type="checkbox" id="remember" />
                            <label htmlFor="remember">
                                Recuérdame
                            </label>
                        </div>
                    </div>

                    {/* /.col */}
                    <div className="col-4">
                        {/*<button type="submit" className="btn btn-primary btn-block">Ingresar</button>*/}
                    </div>
                    {/* /.col */}
                    </div>

                    {/*Boton ingresar */}
                    <div className="social-auth-links text-center mb-3">
                        <button type="submit" className="btn btn-block btn-primary">
                        {/*<i className="fab fa-facebook mr-2" /> */}
                        Ingresar
                        </button>

                        {/**Boton crear cuenta */}
                        <Link to= {"/crear-cuenta"} className="btn btn-block btn-danger">
                        {/*<i className="fab fa-google-plus mr-2" />*/} 
                        Crear cuenta
                        </Link>
                    </div>
                </form>
                
                {/* /.social-auth-links */}
                <p className="mb-1">
                    <Link to ={"#"}>Olvidé mi password</Link>
                </p>
                {/*
                <p className="mb-0">
                    <Link to ={"#"}" className="text-center">Registro nuevo</Link>
                </p>
                */}

                </div>
                    {/* /.login-card-body */}
                </div>
            </div>
        </div>
    )
}

export default Login;